'use strict';

/**
 * @ngdoc service
 * @name travelsApp.currencyConversion
 * @description
 * # currencyConversion
 * Factory in the travelsApp.
 */
angular.module('travelsApp')
  .factory('currencyConversion', function ($http) {
    // Service logic
    // ...
    var newAmmount = 0;

    function  handleSuccess(response) {
        //console.log(response);

        if (typeof response.data !== 'undefined') {
            return ( response.data );
        } else {
            return ( response );
        }
    };

    var getCurrencies=  function(currencyIndex,ammount){
       return $http.get('https://api.fixer.io/latest?base='+currencyIndex)
        .then(handleSuccess);
    }

    // Public API here
    return {
      getCurrencies: getCurrencies
    };
  });
