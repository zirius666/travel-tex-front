'use strict';

angular.module('travelsApp')
    .provider('urls', function () {
        // In the provider function, you cannot inject any
        // service or factory. This can only be done at the
        // "$get" method.

        this.login = '/';
        this.loginUrl = '/login';
        this.logout = '/logout';


        this.$get = function () {
            var login = this.login;
            var logout = this.logout;


            var loginUrl = this.loginUrl;
            return {
                login: function () {
                    return login;
                },
                logout: function () {
                    return logout;
                },

                loginUrl: function () {
                    return loginUrl;
                }
            }
        };

        this.setLogin = function (url) {
            this.login = url;
        };

        this.setLogout = function (url) {
            this.logout = url;
        };

        this.setLoginUrl = function (url) {
            this.loginUrl = url;
        };

    })
    .config(['urlsProvider', function (urlsProvider) {
        var APIURL = 'https://mighty-garden-62797.herokuapp.com';

        urlsProvider.setLogin(APIURL + '/secret');
        urlsProvider.setLogout(APIURL + '/logout');
        // urlsProvider.setLogin('http://soyosung/api/Secret/');
    }])
    .factory('addToken', ['$q', 'currentUser', "$location", "$rootScope",
        function ($q, currentUser, $location, $rootScope) {


            var request = function (config) {

                if (currentUser.profile.loggedIn) {
                    config.headers.Authorization = currentUser.profile.token;
                    $rootScope.logged = true;
                }
                else {
                    $location.path("/");
                }
                /*if*/

                return $q.when(config);
            }

            return {
                request: request
            };

        }
    ])
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push("addToken");
    }])
    .factory('currentUser', ['localStorage', '$window', function (localStorage, $window) {
        // Service logic
        // ...

        var USERKEY = 'utoken';


        var setProfile = function (employeeID, token, role, fullName, clientID) {
            profile.employeeID = employeeID;
            profile.token = token;
            profile.role = role;
            profile.fullName = fullName;
            profile.clientID = clientID;
            profile.currency = currency;
            this.profile.employeeID = employeeID;
            this.profile.token = token;
            this.profile.role = role;
            this.profile.fullName = fullName;
            this.profile.clientID = clientID;
            this.profile.currency = currency;

            localStorage.add(USERKEY, profile);
        };

        var setCurrency = function (currencySelected) {
            currency.selectedCurrency = currencySelected;
            this.currency.selectedCurrency = currencySelected;

            localStorage.add("currency", currency);
        };

        var initialize = function () {
            var user = {
                employeeID: "",
                token: "",
                role: [],
                fullName: "",
                clientID: "",
                currency: "",
                get loggedIn() {
                    return this.token;
                }
            };

            var localUser = localStorage.get(USERKEY);
            if (localUser) {
                user.employeeID = localUser.employeeID;
                user.token = localUser.token;
                user.clientID = localUser.clientID;
                user.role = localUser.role;
                user.fullName = localUser.fullName;
                user.currency = localUser.currency;
            }
            return user;
        };
        var initializeCurrency = function () {
            var currency = {
                defaultCurrency: "MXN",
                selectedCurrency: ""
            }

            var localCurrency = localStorage.get("currency");
            if (localCurrency) {
                currency.selectedCurrency = localCurrency.selectedCurrency;
                currency.defaultCurrency = localCurrency.defaultCurrency;
            }
            return currency;
        };


        var currency = initializeCurrency();
        var profile = initialize();


        return {
            setCurrency: setCurrency,
            currency: currency,
            setProfile: setProfile,
            profile: profile,

        };
    }])
    .factory('oauth', ['$http', '$location', 'currentUser', 'urls',
        function ($http, $location, currentUser, urls) {
            // Service logic
            // ...


            var meaningOfLife = 42;
            var login = function (username, password) {

                var data = {
                    username: username,
                    password: password,
                    appID: urls.appId()
                };
                //return $http.post(urls.login(), data)
                return $http.post(urls.login(), data)
                    .then(function (response) {
                        console.log(response);

                        return response;


                    });
            };


            var logout = function () {
                var clientID = currentUser.profile.clientID,
                    username = currentUser.profile.username,
                    key = currentUser.profile.token;

                currentUser.setProfile(null, null, null, null, null);


                $location.path(urls.loginUrl());
                var data = {
                    appID: urls.appId(),
                    clientId: clientID,
                    username: username,
                    key: key
                };
                $http.post(urls.logout(), data)
                    .then(function (response) {
                        alert("You've been logged out .");
                        localStorage.clear();
                    }, function (error) {
                        alert("Unable to log out ");
                        localStorage.clear();
                    });
            };

            var checkPermissionForView = function (view) {
                if (!view.requiresAuthentication) {
                    return true;
                }

                return userHasPermissionForView(view);
            };


            var userHasPermissionForView = function (view) {
                console.log(currentUser.profile.loggedIn);

                if (!currentUser.profile.loggedIn) {
                    return false;
                }

                if (!view.permissions || !view.permissions.length) {
                    return true;
                }

                return userHasPermission(view.permissions);
            };


            var userHasPermission = function (permissions) {
                if (!currentUser.profile.loggedIn) {
                    return false;
                }

                var found = false;
                angular.forEach(permissions, function (permission, index) {
                    if (currentUser.profile.role.indexOf(permission) >= 0) {


                        found = true;
                        return;
                    }
                });

                return found;
            };


            var isLoggedIn = (currentUser.profile.loggedIn != null &&
            currentUser.profile.loggedIn != undefined &&
            currentUser.profile.loggedIn != "");
            // Public API here
            return {
                login: login,
                logout: logout,
                checkPermissionForView: checkPermissionForView,
                userHasPermission: userHasPermission,
                isLoggedIn: isLoggedIn,

            };
        }
    ])
    .factory('localStorage', ['$window', function ($window) {
        // Service logic
        // ...

        var meaningOfLife = 42;
        var store = $window.localStorage;

        var add = function (key, value) {
            value = angular.toJson(value);
            store.setItem(key, value);
        };

        var get = function (key) {
            var value = store.getItem(key);
            if (value) {
                value = angular.fromJson(value);
            }
            return value;
        };

        var remove = function (key) {
            store.removeItem(key);
        };

        var clear = function () {
            store.clear();
        };

        // Public API here
        return {
            add: add,
            get: get,
            remove: remove,
            clear: clear
        };
    }]);
