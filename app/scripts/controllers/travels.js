'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:TravelsCtrl
 * @description
 * # TravelsCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('ExpenseModalCtrl',
    function ($uibModalInstance, Travel, currentUser,
              Employee, expenseSelected, travelID, maxBudget, SweetAlert, toaster, Upload, $scope, Container) {
      var $ctrl = this;


      $ctrl.expense = {};
      $ctrl.expense.travelID = travelID;
      $ctrl.sending = false;
      $ctrl.expense.date = new Date();

      var maxDate = new Date();
      var duration = 15;
      $ctrl.edit = false;
      if (expenseSelected) {
        console.log(expenseSelected);
        $ctrl.expense = expenseSelected;
        console.log($ctrl.file)
        $ctrl.image = APIURL+"/containers/"+$ctrl.expense.id+"/download/"+$ctrl.expense.file_name;
        $ctrl.expense.date = new Date(expenseSelected.date);
        $ctrl.edit = true;
      }

      console.log(maxBudget);
      maxDate.setTime(maxDate.getTime() + (15 * 24 * 60 * 60 * 1000));

      $ctrl.validateAndSendFile = function () {
        if ($ctrl.file) {

          $ctrl.upload($ctrl.file, $ctrl.container);
        }
      };
      $scope.$watch(() => {
        return $ctrl.file;
      }, (newVal, oldVal, another) => {
        console.log($ctrl.file);

      })
      // upload on file select or drop
      $ctrl.upload = function (file, container) {
        Upload.upload({
          url: APIURL + "/containers/" + container + "/upload",
          data: {file: file}
        }).then(function (resp) {
          toaster.pop("success", "Image saved");
          console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ');
          console.log(resp.data)
        }, function (err) {
          console.log('Error status: ');
          console.log(err);
        }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
      };

      $ctrl.dateOptions = {
        // dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: maxDate,
        minDate: new Date(),
        startingDay: 1
      };


      $ctrl.open1 = function () {
        $ctrl.popup1.opened = true;
      };


      var tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      var afterTomorrow = new Date();
      afterTomorrow.setDate(tomorrow.getDate() + 1);


      $ctrl.popup1 = {
        opened: false
      };


      $ctrl.ok = function () {
        //$uibModalInstance.close($ctrl.selected.item);
      };

      /**
       * @desc function for cancel the modal
       * @param url
       */
      $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };

      /**
       * @desc function for save or update the expense
       * @param url
       */
      $ctrl.saveExpense = function () {
        $ctrl.sending = true;
        console.log($ctrl.expense);
        $ctrl.expense.date_updated = new Date();
        $ctrl.expense.travel_id = travelID;

        Travel.totalOfExpenses({
          idTravel: travelID
        })
          .$promise
          .then((resAmmount) => {
            console.log(resAmmount)
            var total = resAmmount.ammount + $ctrl.expense.ammount;
            console.log(total);
            if (total > maxBudget) {
              SweetAlert.swal({
                  title: "Warning!",
                  text: "You are exceding the maximum budget covered by the company , are you sure?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonColor: "#DD6B55", confirmButtonText: "Yes",
                  cancelButtonText: "No!",
                  closeOnConfirm: true,
                  closeOnCancel: true
                },
                function (isConfirm) {
                  if (isConfirm) {
                    $ctrl.saveOrEditExpense();
                  } else {
                    $uibModalInstance.dismiss('cancel');
                  }
                });
            } else {
              $ctrl.saveOrEditExpense();
            }

          })
          .catch((err) => {
            console.log(err)
          })


      }
      $ctrl.createContainerAndSave = (idExpense) => {
        Container.createContainer({
          name: idExpense
        })
          .$promise
          .then((resCont) => {
            console.log(resCont);
            $ctrl.container = resCont.name;
            $ctrl.validateAndSendFile();
          })
          .catch((errCont) => {
            toaster.pop("error", "Error saving image please try again");
          })
      };
      $ctrl.searchContainerAndSave = (idExpense)=>{
        Container.getContainer({
          container: idExpense
        })
          .$promise
          .then((resCont)=>{
            console.log(resCont);
            $ctrl.container = resCont.name;
            $ctrl.validateAndSendFile();
          })
          .catch((err)=>{
              console.log(err)
          })
      }

      $ctrl.saveOrEditExpense = function () {
        $ctrl.expense.file_name = $ctrl.file.name;
        if (!$ctrl.edit) {
          console.log("create");
          Employee.expenses.create({
            id: currentUser.profile.employeeID
          }, $ctrl.expense)
            .$promise
            .then(function (resExp) {
              console.log(resExp);
              toaster.pop("success", "Request created");

              $uibModalInstance.close(resExp);
              $ctrl.sending = false;

              $ctrl.createContainerAndSave(resExp.id);
            })
            .catch(function (err) {
              toaster.pop("error", "Error saving please try again");

              console.log(err);

            });
        }
        else {
          console.log("edit");

          Employee.expenses.updateById({
            id: currentUser.profile.employeeID,
            fk: $ctrl.expense.id
          }, $ctrl.expense)
            .$promise
            .then(function (resExp) {
              console.log(resExp);
              toaster.pop("success", "Request updated");
              $ctrl.searchContainerAndSave(resExp.id);
              $uibModalInstance.close(resExp);
              $ctrl.sending = false;
            })
            .catch(function (err) {
              toaster.pop("error", "Error saving please try again");

              console.log(err);
            });
        }
      }

    })
  .controller('TravelsCtrl',
    function ($rootScope, $stateParams, Travel, Expense, Employee,
              currentUser, $uibModal, currencyConversion, $scope, $state) {

      var vm = this;
      vm.animationsEnabled = true;
      vm.availablePages = [];
      vm.sort = [];

      console.log($stateParams.travelID);

      vm.isEdit = false;

      if ($stateParams.travelID) {
        vm.isEdit = true;
        vm.travelID = $stateParams.travelID;
      }
      /**
       * @desc function for change the ammount depending the currency
       * @param url
       */
      vm.formatWithCurrency =(ammount)=>{
        if (currentUser.currency.selectedCurrency !== "MXN") {
          return parseFloat(ammount * $rootScope.dataRates.rates[currentUser.currency.selectedCurrency]).toFixed(2) +
            " " + currentUser.currency.selectedCurrency;
        } else {
          return ammount + " MXN";
        }
      };

      /**
       * @desc function for change the ammount depending the currency
       * @param url
       */
      $scope.formatWithCurrency = function (ammount) {

          return  vm.formatWithCurrency(ammount);

      };
      /**
       * @desc function for initialize the view
       * @param url
       */
      vm.init = function () {

        if (vm.isEdit) {
          Employee.travels.findById({
            id: currentUser.profile.employeeID,
            fk: vm.travelID
          })
            .$promise
            .then(function (resTravel) {
              vm.travel = resTravel;
              console.log(resTravel);
              vm.load();
            })
            .catch(function (err) {
              console.log(err);
            })
        }
        else {


          Employee.travels({
            id: currentUser.profile.employeeID,
            filter: {
              order: ['date_created desc'],
              limit: 1
            }
          })
            .$promise
            .then(function (resTravel) {
              console.log(resTravel);
              vm.travel = resTravel[0];
              vm.load();
            })
            .catch(function (err) {
              console.log(err);
            });
        }
      }

      /**
       * @desc function for open the modal of the expenses
       * @param url
       */
      vm.openExpense = function (expense) {
        var expenseToOpen = {};
        expenseToOpen = _.clone(expense);
        var parentElem = undefined;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: '../views/modals/expenseModal.html',
          controller: 'ExpenseModalCtrl',
          controllerAs: '$ctrl',
          size: "md",
          appendTo: parentElem,
          resolve: {
            expenseSelected: function () {
              return expense ? expenseToOpen : null;
            },
            travelID: function () {
              return vm.travel.id;

            },
            maxBudget: function () {
              return vm.travel.max_budget;
            }
          }
        });

        modalInstance.result.then(function (resModal) {
          vm.load();
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      };

      //Object to setup the pagination of ui- grid
      vm.pagination = {
        pageSize: 10,
        pageNumber: 1,
        totalItems: null,
        getTotalPages: function () {
          vm.availablePages = [];
          let totalPages = Math.ceil(this.totalItems / this.pageSize);
          for (let i = 0; i < totalPages; i++) {
            vm.availablePages.push(i + 1);
          }

          return totalPages;

        },
        nextPage: function () {
          if (this.pageNumber < this.getTotalPages()) {
            this.pageNumber++;
            vm.load();
          }
        },
        previousPage: function () {
          if (this.pageNumber > 1) {
            this.pageNumber--;
            vm.load();
          }
        }
      };

      /**
       * @desc function for load the records depending on filters
       * @param
       */
      vm.load = function () {
        console.log([vm.pagination.pageSize, vm.pagination.pageNumber, vm.sort, vm.where]);
        vm.filter = {};
        vm.filter.where = vm.where;
        vm.filter.limit = vm.pagination.pageSize;
        vm.filter.skip = (vm.pagination.pageNumber - 1) * vm.pagination.pageSize;
        vm.filter.order = vm.sort;
        console.log(vm.filter);


        Travel.expenses({
          id: vm.travel.id,
          filter: vm.filter
        })
          .$promise
          .then(function (resExpenses) {
            Travel.expenses.count({
              id: vm.travel.id
            })
              .$promise
              .then(function (resCount) {
                console.log(resCount);
                console.log(resExpenses);
                vm.gridOptions.data = resExpenses;
                vm.pagination.totalItems = resCount.count;
              })
              .catch(function (err) {

              });


          })
          .catch(function (err) {
            console.log(err);
          });

      };

      //OBject to setup the options of ui-grid
      vm.gridOptions = {
        excludeProperties: '__metadata',
        enablePaginationControls: false,
        useExternalSorting: true,
        useExternalFiltering: true,
        enableFiltering: true,
        columnDefs: [

          {name: 'type', width: 130},
          {
            name: 'date',
            width: 140,
            enableFiltering: false,
            displayName: 'Date created',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatDate(COL_FIELD)}}</p>'
          },

          {
            name: 'ammount', width: 140,
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatWithCurrency(COL_FIELD)}}</p>'
          },
          {
            name: 'id',
            width: 90,
            displayName: 'Actions',
            enableFiltering: false,
            cellTemplate: '<div style="text-align: center;margin-bottom: 15px;">' +
            '<button style="height: 25px !important;vertical-align: text-top;margin-right:5px !important;" class="btn btn-primary btn-sm" id="fila" ' +
            'ng-click="grid.appScope.goto(\'{{COL_FIELD }}\' )">' +
            '<i class="fa fa-eye"></i></button>' +
            '<button style="height: 25px !important;vertical-align: text-top;" class="btn btn-success btn-sm" id="fila" ' +
            'ng-click="grid.appScope.edit( row.entity)">' +
            '<i class="fa fa-pencil"></i></button>' +
            '</div>'
          },
        ],
        onRegisterApi: function (gridApi) {
          vm.gridApi = gridApi;
          //declare the events

          vm.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
            vm.sort = [];
            angular.forEach(sortColumns, function (sortColumn) {
              //  vm.sort.push({fieldName: sortColumn.name, order: sortColumn.sort.direction});
              vm.sort.push(sortColumn.name + " " + sortColumn.sort.direction);
            });


            vm.load();
          });

          vm.gridApi.core.on.filterChanged($scope, function () {
            //vm.filter = [];
            vm.where = {};
            var grid = this.grid;
            angular.forEach(grid.columns, function (column) {
              var fieldName = column.field;
              var value = column.filters[0].term;
              var operator = "like";
              if (value) {
                if (fieldName == "id") operator = "equals";
                else if (fieldName == "ammount") operator = "gte";
                /*vm.filter.push({
                 fieldName: fieldName,
                 operator: operator,
                 value: value
                 });*/
                if(operator == "like"){
                  vm.where[fieldName] = {
                    [operator]: value,
                    options: 'i'
                  };
                }else{
                  vm.where[fieldName] = {
                    [operator]: value
                  };
                }
              }
            });

            vm.load();
          });
        }
      };
      /**
       * @desc function for change location depending of url
       * @param url
       */
      $scope.goto = (url) => {
        console.log("goto");
        vm.goto(url);
        // $location.path(parser.pathname).search(params);
      };

      vm.goto = (id)=>{
        $state.go('expenses', {expenseID: id, travelID: vm.travel.id});
      };

      /**
       * @desc function for format the dates from ui grid
       * @param date
       */
      $scope.formatDate = (date) => {
        return vm.formatDate(date);
      };
      /**
       * @desc function for format the dates from actual scope
       * @param url
       */
      vm.formatDate = (date) => {
        return moment(date).format("DD-MM-YY HH:mm");
      };
      /**
       * @desc function for open the modals of the travel
       * @param expense
       */
      $scope.edit = (expenseSelected) => {
        vm.openExpense(expenseSelected);
      };

      vm.openTravel = (travelSelected) => {
        var travelToOpen = {};
        travelToOpen = _.clone(travelSelected);
        var parentElem = undefined;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: '../views/modals/travelModal.html',
          controller: 'modalTravelCtrl',
          controllerAs: '$ctrl',
          size: "md",
          appendTo: parentElem,
          resolve: {
            travelSelected: function () {
              return travelSelected ? travelToOpen : null;
            }
          }
        });

        modalInstance.result.then(function (resModal) {
          vm.travel = resModal;
          vm.load();
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      };
      vm.init();


    });
