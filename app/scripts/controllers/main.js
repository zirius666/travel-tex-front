'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('MainCtrl', function($rootScope, Account, Travel, $location, $state, oauth, currentUser,toaster) {
    var vm = this;
    $rootScope.logged = false;
    currentUser.setProfile(null, null, null, null, null);

    vm.isSending = false;
    /**
     * @desc function forlogin
     * @param url
     */
    vm.login = function() {
      vm.isSending = true;
      console.log(vm.user);
      Account.login(vm.user,
        function(res) {
          vm.isSending = false;
          console.log(res);
          $state.go("dashboard");
          $rootScope.logged = true;
          currentUser.setProfile(null, res.id, null, null, res.userId);
          currentUser.setCurrency("MXN");
          console.log("location dashboard");
        },
        function(err) {
          toaster.pop("error","Invalid credentials try again");
          vm.isSending = false;
          console.log(err);
        })
    };




  });
