'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:ExpensesCtrl
 * @description
 * # ExpensesCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('ExpensesCtrl', function ($rootScope, $stateParams, Travel, Expense,
                                        currentUser, $uibModal, currencyConversion, $scope, $state,Container) {

    var vm = this;
    //we will evaluate if we are receiving the expense id
    if ($stateParams.expenseID) {
      vm.isEdit = true;
      vm.expenseID = $stateParams.expenseID;
      vm.travelID = $stateParams.travelID;
      vm.travel = undefined;
    }
    /**
     * @desc function to format dates
     * @param url
     */
    vm.formatDate = (date) => {
      return moment(date).format("DD-MM-YY HH:mm");
    };

    /**
     * @desc function for initialize the data of the view
     * @param url
     */
    vm.init = () => {
      if (vm.isEdit) {
        Expense.findById({
          id: vm.expenseID,

        })
          .$promise
          .then(function (resExp) {
            vm.expense = resExp;
            console.log(resExp);
            Travel.findById({
              id: vm.travelID
            })
              .$promise
              .then((resTravel) => {
                console.log(resTravel)
                vm.travel = resTravel;
                vm.image = APIURL+"/containers/"+vm.expense.id+"/download/"+vm.expense.file_name;

              })
              .catch((errTravel) => {

              })
          })
          .catch(function (err) {
            console.log(err);
          })
      }

    };
    /**
     * @desc function for open the modal of the expenses
     * @param url
     */
    vm.openExpense = function (expense) {
      var expenseToOpen = {};
      expenseToOpen = _.clone(expense);
      var parentElem = undefined;
      var modalInstance = $uibModal.open({
        animation: vm.animationsEnabled,
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: '../views/modals/expenseModal.html',
        controller: 'ExpenseModalCtrl',
        controllerAs: '$ctrl',
        size: "md",
        appendTo: parentElem,
        resolve: {
          expenseSelected: function () {
            return expense ? expenseToOpen : null;
          },
          travelID: function () {
            return vm.travelID;

          },
          maxBudget: function () {
            return vm.travel.max_budget;
          }
        }
      });

      modalInstance.result.then(function (resModal) {
        vm.expense = resModal;
        vm.image = APIURL+"/containers/"+vm.expense.id+"/download/"+vm.expense.file_name;
      }, function () {
        console.log('Modal dismissed at: ' + new Date());
      });
    };
    /**
     * @desc function for change the ammount depending the currency
     * @param url
     */
    $scope.formatWithCurrency = function (ammount) {


      if (currentUser.currency.selectedCurrency !== "MXN") {
        return parseFloat(ammount * $rootScope.dataRates.rates[currentUser.currency.selectedCurrency]).toFixed(2) +
          " " + currentUser.currency.selectedCurrency;
      } else {
        return ammount + " MXN";
      }
    };
    vm.formatWithCurrency = (value) => {
      return $scope.formatWithCurrency(value);
    };
    //Execute the call to load the basic data of the view
    vm.init();
  });
