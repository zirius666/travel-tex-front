'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:NavbarCtrl
 * @description
 * # NavbarCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('NavbarCtrl', function ($rootScope, $scope, currentUser) {

    var vm =this;

    vm.onChangeCurrency = () => {
      console.log("Change currency...");
    }

    $scope.$watch(()=>{
        return $rootScope.selectedCurrency;
    },(newVal, oldVal, another)=>{
        console.log($rootScope.selectedCurrency);

        if($rootScope.selectedCurrency!==undefined){
          currentUser.setCurrency($rootScope.selectedCurrency);

        }
    })



  });
