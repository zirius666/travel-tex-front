'use strict';

/**
 * @ngdoc function
 * @name travelsApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the travelsApp
 */
angular.module('travelsApp')
  .controller('modalTravelCtrl', function ($uibModalInstance, Travel, currentUser, Employee, travelSelected, toaster) {
    var $ctrl = this;

    $ctrl.travel = {};
    $ctrl.travel.employeeID = currentUser.profile.employeeID;
    $ctrl.sending = false;
    $ctrl.travel.date_begin = new Date();
    $ctrl.travel.date_end = new Date();
    var maxDate = new Date();
    var duration = 15;
    $ctrl.edit = false;

    //We are evaluating if we receive some travel like parameter
    if (travelSelected) {
      $ctrl.travel = travelSelected;
      $ctrl.travel.date_begin = new Date(travelSelected.date_begin);
      $ctrl.travel.date_end = new Date(travelSelected.date_end);
      $ctrl.edit = true;
    }

    console.log(travelSelected);
    maxDate.setTime(maxDate.getTime() + (15 * 24 * 60 * 60 * 1000));

    //configuration for date picker
    $ctrl.dateOptions = {
      // dateDisabled: disabled,
      formatYear: 'yy',
      maxDate: maxDate,
      minDate: new Date(),
      startingDay: 1
    };

    //handler to restrict the pickers to avoid error
    $ctrl.changeDate2 = function () {
      console.log($ctrl.travel.date_end);
      var start = moment($ctrl.travel.date_begin);
      var end = moment($ctrl.travel.date_end);
      if (start > end) {
        console.log("is after");
        $ctrl.travel.date_begin = $ctrl.travel.date_end;
      }
      //
    };

    $ctrl.open1 = function () {
      $ctrl.popup1.opened = true;
    };

    $ctrl.open2 = function () {
      $ctrl.popup2.opened = true;
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);

    $ctrl.popup2 = {
      opened: false
    };
    $ctrl.popup1 = {
      opened: false
    };


    $ctrl.ok = function () {
      //$uibModalInstance.close($ctrl.selected.item);
    };

    $ctrl.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };

    /**
     * @desc function for create or update the travel
     * @param url
     */
    $ctrl.createTravel = function () {
      $ctrl.sending = true;
      console.log($ctrl.travel);
      $ctrl.travel.date_updated = new Date();
      if (!$ctrl.edit) {
        $ctrl.travel.date_created = new Date();
        console.log("create");
        Employee.travels.create({id: currentUser.profile.employeeID}, $ctrl.travel)
          .$promise
          .then(function (res) {
            toaster.pop("success", "Request created correctly");
            console.log(res);
            $uibModalInstance.close(res);
            $ctrl.sending = false;
          })
          .catch(function (err) {
            toaster.pop("error", "Error saving please try again");
            console.log(err);
            $ctrl.sending = false;
          })
      }
      else {
        console.log("edit");

        Employee.travels.updateById({
            id: currentUser.profile.employeeID,
            fk: $ctrl.travel.id
          },
          $ctrl.travel)
          .$promise
          .then(function (res) {
            toaster.pop("success", "Request updated");
            console.log(res);
            $uibModalInstance.close(res);
            $ctrl.sending = false;
          })
          .catch(function (err) {
            toaster.pop("error", "Error saving please try again");

            console.log(err);
            $ctrl.sending = false;
          })
      }


    }

  })
  .controller('DashboardCtrl',
    function (Travel, $uibModal, $log, $document, $scope, currentUser,
              Employee, Account, $state, $rootScope) {

      var vm = this;

      console.log("dashboard");

      vm.availablePages = [];
      vm.sort = [];

      //object to setup the pagination
      vm.pagination = {
        pageSize: 10,
        pageNumber: 1,
        totalItems: null,
        getTotalPages: function () {
          vm.availablePages = [];
          let totalPages = Math.ceil(this.totalItems / this.pageSize);
          for (let i = 0; i < totalPages; i++) {
            vm.availablePages.push(i + 1);
          }

          return totalPages;

        },
        nextPage: function () {
          if (this.pageNumber < this.getTotalPages()) {
            this.pageNumber++;
            vm.load();
          }
        },
        previousPage: function () {
          if (this.pageNumber > 1) {
            this.pageNumber--;
            vm.load();
          }
        }
      };
      /**
       * @desc function for load the last activity
       * @param url
       */
      vm.loadLastActivity = () => {
        Employee.travels({
          id: currentUser.profile.employeeID,
          filter: {
            order: ['date_updated desc'],
            limit: 1
          }
        })
          .$promise
          .then(function (resTravels) {

            if (resTravels[0] != undefined) {
              vm.last_travel = resTravels[0];
              Employee.expenses({
                id: currentUser.profile.employeeID,
                filter: {
                  order: ['date_updated desc'],
                  limit: 1
                }
              })
                .$promise
                .then((resLasExp) => {

                  if (resLasExp[0] !== undefined) {
                    vm.last_expense = resLasExp[0];
                    console.log([vm.last_expense, vm.last_travel]);
                    var updateExpense = moment(vm.last_expense.date_updated);
                    var updateTravel = moment(vm.last_travel.date_updated);
                    console.log([updateExpense, updateTravel])
                    if (updateTravel < updateExpense) {
                      vm.activityTravel = false;
                    } else {
                      vm.activityTravel = true;
                    }
                  } else {
                    vm.activityTravel = true;
                  }


                })
                .catch((errExp) => {
                  console.log(errExp)
                })
            }


          })
          .catch(function (err) {
            console.log(err);

          })
      }

      /**
       * @desc function for load the data depending filters
       * @param url
       */
      vm.load = function () {
        console.log([vm.pagination.pageSize, vm.pagination.pageNumber, vm.sort, vm.where]);
        vm.filter = {};
        vm.filter.where = vm.where;
        vm.filter.limit = vm.pagination.pageSize;
        vm.filter.skip = (vm.pagination.pageNumber - 1) * vm.pagination.pageSize;
        vm.filter.order = vm.sort;
        console.log(vm.filter);


        Employee.travels({
          id: currentUser.profile.employeeID,
          filter: vm.filter
        })
          .$promise
          .then(function (resTravels) {
            Employee.travels.count({
              id: currentUser.profile.employeeID
            })
              .$promise
              .then(function (resCount) {
                console.log(resCount);
                console.log(resTravels);
                vm.gridOptions.data = resTravels;
                vm.pagination.totalItems = resCount.count;
                vm.loadLastActivity();
              })
              .catch(function (err) {

              });


          })
          .catch(function (err) {
            console.log(err);
          });

      };

      //object to setup the grid
      vm.gridOptions = {
        excludeProperties: '__metadata',
        enablePaginationControls: false,
        useExternalSorting: true,
        useExternalFiltering: true,
        enableFiltering: true,
        columnDefs: [

          {name: 'destination', width: 130},
          {
            name: 'date_begin',
            width: 110,
            enableFiltering: false,
            displayName: 'Begins',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatDate(COL_FIELD)}}</p>'
          },
          {
            name: 'date_end',
            width: 110,
            enableFiltering: false,
            displayName: 'Ends',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatDate(COL_FIELD)}}</p>'
          },
          {
            name: 'date_created',
            width: 110,
            enableFiltering: false,
            displayName: 'Created',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatDate(COL_FIELD)}}</p>'
          },
          {
            name: 'required_ammount', width: 140, displayName: 'Req. ammount',
            cellTemplate: '<p class="margint-top-5">{{ grid.appScope.formatWithCurrency(COL_FIELD)}}</p>'
          },
          {name: 'project', width: 90},
          {name: 'customer', width: 220},
          {
            name: 'id',
            width: 90,
            displayName: 'Actions',
            enableFiltering: false,
            cellTemplate: '<div style="text-align: center;margin-bottom: 15px;">' +
            '<button style="height: 25px !important;vertical-align: text-top;margin-right:5px !important;" class="btn btn-primary btn-sm" id="fila" ' +
            'ng-click="grid.appScope.goto(\'{{COL_FIELD }}\' )">' +
            '<i class="fa fa-eye"></i></button>' +
            '<button style="height: 25px !important;vertical-align: text-top;" class="btn btn-success btn-sm" id="fila" ' +
            'ng-click="grid.appScope.edit( row.entity)">' +
            '<i class="fa fa-pencil"></i></button>' +
            '</div>'
          },
        ],
        onRegisterApi: function (gridApi) {
          vm.gridApi = gridApi;
          //declare the events

          vm.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
            vm.sort = [];
            angular.forEach(sortColumns, function (sortColumn) {
              //  vm.sort.push({fieldName: sortColumn.name, order: sortColumn.sort.direction});
              vm.sort.push(sortColumn.name + " " + sortColumn.sort.direction);
            });


            vm.load();
          });

          vm.gridApi.core.on.filterChanged($scope, function () {
            //vm.filter = [];
            vm.where = {};
            var grid = this.grid;
            angular.forEach(grid.columns, function (column) {
              var fieldName = column.field;
              var value = column.filters[0].term;
              var operator = "like";
              if (value) {
                if (fieldName == "id") operator = "equals";
                else if (fieldName == "required_ammount") operator = "gte";

                /*vm.filter.push({
                 fieldName: fieldName,
                 operator: operator,
                 value: value
                 });*/
                if(operator == "like"){
                  vm.where[fieldName] = {
                    [operator]: value,
                    options: 'i'
                  };
                }else{
                  vm.where[fieldName] = {
                    [operator]: value
                  };
                }

              }
            });

            vm.load();
          });
        }
      };

      /**
       * @desc function for setup the data of the view
       * @param url
       */
      vm.init = function () {
        Account.employee({id: currentUser.profile.clientID})
          .$promise
          .then(function (res) {
            console.log(res);
            vm.employee = res;
            currentUser.setProfile(res.id, currentUser.profile.token, null, null, currentUser.profile.clientID);
            vm.load();

          })
          .catch(function (err) {
            console.log(err);
          });


      }


      /**
       * @desc function for change location depending of url
       * @param url
       */
      $scope.goto = function (url) {
        console.log("goto");
        $state.go('travels', {travelID: url});
        // $location.path(parser.pathname).search(params);
      };
      /**
       * @desc function for change the ammount depending the currency
       * @param url
       */
      $scope.formatWithCurrency = function (ammount) {


        if (currentUser.currency.selectedCurrency !== "MXN") {
          return parseFloat(ammount * $rootScope.dataRates.rates[currentUser.currency.selectedCurrency]).toFixed(2) +
            " " + currentUser.currency.selectedCurrency;
        } else {
          return ammount + " MXN";
        }
      };
      /**
       * @desc function for change the ammount depending the currency
       * @param value
       */
      vm.formatWithCurrency = (value) => {
        return $scope.formatWithCurrency(value);
      };

      $scope.edit = function (travelSelected) {

        vm.open(travelSelected);

      };
      $scope.formatDate = (date) => {
        return moment(date).format("DD-MM-YY HH:mm");
      };
      /**
       * @desc function for return date formatted from view
       * @param value
       */
      vm.formatDate = (value) => {
        return $scope.formatDate(value);
      };


      vm.animationsEnabled = true;

      /**
       * @desc function for open the modal of the travel
       * @param url
       */
      vm.open = function (travelSelected) {
        var parentElem = undefined;
        var modalInstance = $uibModal.open({
          animation: vm.animationsEnabled,
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          templateUrl: '../views/modals/travelModal.html',
          controller: 'modalTravelCtrl',
          controllerAs: '$ctrl',
          size: "md",
          appendTo: parentElem,
          resolve: {
            travelSelected: function () {
              return travelSelected ? travelSelected : null;
            }
          }
        });

        modalInstance.result.then(function (resModal) {
          vm.load();

        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
      };

      vm.init();

    });
