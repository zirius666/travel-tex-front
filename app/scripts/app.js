"use strict";

/**
 * @ngdoc overview
 * @name travelsApp
 * @description
 * # travelsApp
 *
 * Main module of the application.
 */
angular
    .module("travelsApp", [
        "ngAnimate",
        "ngCookies",
        "ngMessages",
        "ngResource",
        "ngRoute",
        "ngSanitize",
        "ngTouch",
        "lbServices",
        "ui.bootstrap",
        "ui.router",
        "toaster",
        'ui.grid',
        'angularMoment',
        "oitozero.ngSweetAlert",
        'ngFileUpload'
    ])
    .config(["$locationProvider", function($locationProvider) {
        $locationProvider.hashPrefix("");
    }])
    .run(function($rootScope, oauth, currencyConversion, currentUser) {

        $rootScope.logged = oauth.isLoggedIn ? true : false;
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                // do something
                console.log("Get currencies...");
                if ($rootScope.currencies == undefined || $rootScope.selectedCurrency==undefined) {
                    $rootScope.currencies =[];
                    currencyConversion.getCurrencies("MXN", 1)
                        .then((res) => {
                            console.log(res);
                            angular.forEach(res.rates,(value, key)=>{
                                $rootScope.currencies.push(key);

                            });
                            $rootScope.dataRates = res;
                            $rootScope.currencies.push("MXN");
                            console.log(currentUser.currency.selectedCurrency);
                          if(currentUser.currency.selectedCurrency != undefined){
                            $rootScope.selectedCurrency = currentUser.currency.selectedCurrency;
                          }else{
                            $rootScope.selectedCurrency = 'MXN';
                          }

                            console.log($rootScope.currencies)
                        })
                        .catch((err) => {
                            console.log(err);
                        })
                }

            })
    })
    .config(["$stateProvider", "$urlRouterProvider", "$sceProvider",
        function($stateProvider, $urlRouterProvider, $sceProvider) {
            $sceProvider.enabled(false);
            $urlRouterProvider.otherwise("/");

            $stateProvider
                .state("main", {
                    url: "/",
                    templateUrl: "/views/main.html",
                    controller: "MainCtrl",
                    controllerAs: "main"
                })
                .state("dashboard", {
                    url: "/dashboard",
                    templateUrl: "/views/dashboard.html",
                    controller: "DashboardCtrl",
                    controllerAs: "dashboard"
                })
                .state("travels", {
                    url: "/travels/:travelID",
                    templateUrl: "views/travels.html",
                    controller: "TravelsCtrl",
                    controllerAs: "travels"
                })
                .state("expenses", {
                    url: "/expenses/:expenseID/:travelID",
                    templateUrl: "views/expenses.html",
                    controller: "ExpensesCtrl",
                    controllerAs: "expenses"
                });
            /* .otherwise({
             redirectTo: "/"
             });*/
        }
    ]);
