'use strict';


var APIURL = 'https://nameless-river-54607.herokuapp.com/api';
var ISINPRODUCTION = true;

angular
  .module('travelsApp')
  .run(['$rootScope','oauth',function($rootScope,oauth) {
    $rootScope.urlPrefix = APIURL;


  }])
  .config(function(LoopBackResourceProvider,$httpProvider) {

    // Use a custom auth header instead of the default 'Authorization'
    LoopBackResourceProvider.setAuthHeader('X-Access-Token');

    // Change the URL where to access the LoopBack REST API server
    LoopBackResourceProvider.setUrlBase(APIURL);

    $httpProvider.interceptors.push(function($q, $location, LoopBackAuth) {
      return {
        responseError: function(rejection) {
          if (rejection.status == 401) {
            //Now clearing the loopback values from client browser for safe logout...
            LoopBackAuth.clearUser();
            LoopBackAuth.clearStorage();
            $location.nextAfterLogin = $location.path();
            $location.path('/');
          }
          return $q.reject(rejection);
        }
      };
    });
  });

