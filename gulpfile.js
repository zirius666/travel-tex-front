// Generated on 2017-08-18 using generator-angular 0.16.0
'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var openURL = require('open');
var lazypipe = require('lazypipe');
var rimraf = require('rimraf');
var wiredep = require('wiredep').stream;
var runSequence = require('run-sequence');
var bs = require("browser-sync").create();


var yeoman = {
  app: require('./bower.json').appPath || 'app',
  dist: '../travels-tex/client/'
};

var paths = {
  scripts: [yeoman.app + '/scripts/**/*.js'],
  styles: [yeoman.app + '/styles/**/*.css'],
  test: ['test/spec/**/*.js'],
  testRequire: [
    yeoman.app + '/bower_components/angular/angular.js',
    yeoman.app + '/bower_components/angular-mocks/angular-mocks.js',
    yeoman.app + '/bower_components/angular-resource/angular-resource.js',
    yeoman.app + '/bower_components/angular-cookies/angular-cookies.js',
    yeoman.app + '/bower_components/angular-sanitize/angular-sanitize.js',
    yeoman.app + '/bower_components/angular-route/angular-route.js',
    'test/mock/**/*.js',
    'test/spec/**/*.js'
  ],
  karma: 'test/karma.conf.js',
  views: {
    main: yeoman.app + '/index.html',
    files: [yeoman.app + '/views/**/*.html']
  }
};

////////////////////////
// Reusable pipelines //
////////////////////////

var lintScripts = lazypipe()
  .pipe($.jshint, '.jshintrc')
  .pipe($.jshint.reporter, 'jshint-stylish');

var styles = lazypipe()
    .pipe($.autoprefixer, 'last 1 version')
    .pipe(gulp.dest, '.tmp/styles');

///////////
// Tasks //
///////////

gulp.task('styles', function () {
    return gulp.src(paths.styles)
        .pipe(styles());
});

gulp.task('lint:scripts', function () {
  return gulp.src(paths.scripts)
    .pipe(lintScripts());
});

gulp.task('clean:tmp', function (cb) {
  rimraf('./.tmp', cb);
});

gulp.task('start:client', ['start:server', 'styles'], function () {
  openURL('http://localhost:9000');
});

gulp.task('start:server', function() {
  bs.init({
    server:  {
            baseDir: "./app",
        },
    port: process.env.PORT
  });
});

gulp.task('start:server:test', function() {
  $.connect.server({
    root: ['test', yeoman.app, '.tmp'],
    livereload: true,
    port: 9001
  });
});

gulp.task('watch', function () {
  bs.init({
        port: 8080,
        server: {
            baseDir: "app/"
        }
    });
    $.watch(paths.styles, ['styles'], bs.reload);

    $.watch(paths.views.files, bs.reload);

    $.watch(paths.scripts, bs.reload);

    $.watch(paths.test)
        .pipe($.plumber());

    gulp.watch('bower.json', ['bower']);
});

gulp.task('serve', function (cb) {
  runSequence('clean:tmp',
    ['lint:scripts'],
    ['start:client'],
    'watch', cb);
});

gulp.task('serve:prod', function() {
   bs({
        port: 8080,
        server: {
            baseDir: "dist/"
        }
    });
});

gulp.task('test', ['start:server:test'], function () {
  var testToFiles = paths.testRequire.concat(paths.scripts, paths.test);
  return gulp.src(testToFiles)
    .pipe($.karma({
      configFile: paths.karma,
      action: 'watch'
    }));
});

// inject bower components
gulp.task('bower', function () {
  return gulp.src(paths.views.main)
    .pipe(wiredep({
      directory: yeoman.app + '/bower_components',
      ignorePath: '..',
      onFileUpdated: function (filePath) {
                // filePath = 'name-of-file-that-was-updated'
          console.log(filePath);
      },
      onPathInjected: function (fileObject) {
          // fileObject.block = 'type-of-wiredep-block' ('js', 'css', etc)
          // fileObject.file = 'name-of-file-that-was-updated'
          // fileObject.path = 'path-to-file-that-was-injected'
          console.log(fileObject);
      },
    }))
    .pipe(gulp.dest(yeoman.app));
});

///////////
// Build //
///////////

gulp.task('clean:dist', function (cb) {
  rimraf('./dist', cb);
});

// gulp.task('client:build', ['html', 'styles'], function () {
//   var jsFilter = $.filter('**/*.js');
//   var cssFilter = $.filter('**/*.css');

//   return gulp.src(paths.views.main)
//     .pipe($.useref({searchPath: [yeoman.app, '.tmp']}))
//     .pipe(jsFilter)
//     .pipe($.ngAnnotate())
//     .pipe($.uglify())
//     .pipe(jsFilter.restore())
//     .pipe(cssFilter)
//     .pipe($.minifyCss({cache: true}))
//     .pipe(cssFilter.restore())
//     .pipe($.rev())
//     .pipe($.revReplace())
//     .pipe(gulp.dest(yeoman.dist));
// });
gulp.task('client:build',
    ['html',
        'copy:fonts',
        'images',
        // 'clean:dist',
        'styles'], function () {
        var jsFilter = $.filter(['**/*.js',"!app/scripts/**/*.js"]);
        var cssFilter = $.filter('**/*.css');
        var indexHtmlFilter = $.filter(['**/*', '!**/index.html'], { restore: true });
        return gulp.src(paths.views.main)

            .pipe($.useref({
                searchPath: [yeoman.app, '.tmp'],
                 transformPath: function(filePath) {
                     console.log(filePath.replace("bower_components","app/bower_components"));
                     return filePath.replace("bower_components","app/bower_components");
                 }
            }))
            .pipe(jsFilter)
           // .pipe($.ngAnnotate())
           // .pipe($.uglify({mangle:false}))

            .pipe(jsFilter.restore())

            .pipe(cssFilter)
            .pipe($.minifyCss({cache: true}))

            .pipe(cssFilter.restore())
            .pipe(indexHtmlFilter)

            .pipe($.rev())
            .pipe(indexHtmlFilter.restore())
            .pipe($.revReplace())

            .pipe(gulp.dest(yeoman.dist));
    });

gulp.task('html', function () {
  return gulp.src(yeoman.app + '/views/**/*')
    .pipe(gulp.dest(yeoman.dist + '/views'));
});

gulp.task('images', function () {
  return gulp.src(yeoman.app + '/images/**/*')
    .pipe($.cache($.imagemin({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
    })))
    .pipe(gulp.dest(yeoman.dist + '/images'));
});

gulp.task('copy:extras', function () {
  return gulp.src(yeoman.app + '/*/.*', { dot: true })
    .pipe(gulp.dest(yeoman.dist));
});

gulp.task('copy:fonts', function () {
  return gulp.src(yeoman.app + '/fonts/**/*')
    .pipe(gulp.dest(yeoman.dist + '/fonts'));
});

gulp.task('build', ['clean:dist'], function () {
  runSequence(['images', 'copy:extras', 'copy:fonts', 'client:build']);
});

gulp.task('default', ['build']);
