'use strict';

describe('Controller: TravelsCtrl', function () {

  // load the controller's module
  beforeEach(module('travelsApp'));

  var TravelsCtrl,
    scope ,  http , location , mockEmployeeService ,filter , current ,ammount,state;

  var mockEmployee = { id: 133, name: "Max Burg" };
  var mockEmployeeTravel =
    { id: 11, name: "Max Burg" ,
      destination: "Dallas",
      date_begin:"2017-08-24T22:10:32.654Z",
      required_ammount:3000};
  var mockExpense = {
    id:3,
    ammount:444,
    travel_id:11,
    type:"food"

  }
  var mockArrayTravels = [ mockEmployeeTravel];

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _$httpBackend_  , currentUser,Employee,$state) {
    http = _$httpBackend_;
    scope = {};
    state = $state;
    current = currentUser;
    filter = { id: "11",
      filter: {
        order: ['date_created desc'],
        limit: 1
      }
    };
    mockEmployeeService  = Employee;
   // spyOn(mockEmployeeService,"travels");
    spyOn($state,"go");
    current.setProfile(10,10,null,10,10);
    current.setCurrency("MXN");
    TravelsCtrl = $controller('TravelsCtrl', {
      $scope: scope,
      currentUser: current,
      $state: state,
      Employee: mockEmployeeService
      // place here mocked dependencies
    });
  }));


  it("should format the ammount with the actual currency",function () {
    var stringCurrency = TravelsCtrl.formatWithCurrency(mockEmployeeTravel.required_ammount);
    expect(stringCurrency).toEqual("3000 MXN");
  });

  it("should format date ",function () {
    var dateFormated = TravelsCtrl.formatDate(mockEmployeeTravel.date_begin);
    expect(dateFormated).toEqual("24-08-17 17:10");
  });
  it("should redirect to another view",function () {
    TravelsCtrl.travel = mockEmployeeTravel;
    TravelsCtrl.goto(mockExpense.id);

    expect(state.go).toHaveBeenCalledWith("expenses",{ expenseID: mockExpense.id, travelID: mockEmployeeTravel.id });
  })


});
