'use strict';

describe('Controller: ExpensesCtrl', function () {

  // load the controller's module
  beforeEach(module('travelsApp'));

  var ExpensesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ExpensesCtrl = $controller('ExpensesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));


});
