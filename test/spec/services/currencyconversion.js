'use strict';

describe('Service: currencyConversion', function () {

  // load the service's module
  beforeEach(module('travelsApp'));

  // instantiate service
  var currencyConversion;
  beforeEach(inject(function (_currencyConversion_) {
    currencyConversion = _currencyConversion_;
  }));

  it('should do something', function () {
    expect(!!currencyConversion).toBe(true);
  });

});
